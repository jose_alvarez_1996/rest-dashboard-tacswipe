import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import FlatButton from 'material-ui/FlatButton';
import ContentCreate from 'material-ui/svg-icons/content/create';

const EditButton = ({ basePath = '', label = 'aor.action.edit', record = {}, translate }) => <FlatButton
    primary
    label={label}
    icon={<ContentCreate />}
    containerElement={<Link to={`${basePath}/${encodeURIComponent(record._id)}`} />}
    style={{ overflow: 'inherit' }}
/>;

EditButton.propTypes = {
    basePath: PropTypes.string,
    label: PropTypes.string,
    record: PropTypes.object,
};


export default (EditButton);
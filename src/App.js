
import React from 'react';
import { fetchUtils, jsonServerRestClient, Admin, Resource, Delete } from 'admin-on-rest';

import { ReportedListings, ListingsEdit, ListingsCreate, ListingShow } from './reportedListings';
// import { ReportedUsers, EditUsers, CreateUsers } from './reportedUsers';
import { UserList } from './users';
import Dashboard from './Dashboard';
import authClient from './authClient';
import httpClient from './httpClient';
import { allListings } from './Listings';

import ListingsIcon from 'material-ui/svg-icons/action/book';
import UserIcon from 'material-ui/svg-icons/social/group';


const customClient = (url, options = {}) => {
    if (!options.headers) {
            options.headers = new Headers({ Accept: 'application/json' })
        }
        console.log(localStorage.getItem('access-token'));
        options.headers.set('x-access-token', localStorage.getItem('access-token'))
        options.headers.set('x-access-device-id', 'admin-console');
    return fetchUtils.fetchJson(url, options);
}
 const RESTClient = httpClient('https://tacswipeapi.xyz', customClient);
//jsonServerRestClient('http://jsonplaceholder.typicode.com')
// const TESTClient = httpClient('http://localhost:8080', customClient);
const App = () => (
    <Admin authClient={authClient}
      dashboard={Dashboard} restClient={RESTClient}>
        <Resource name="Listings" list={allListings} remove={Delete} />
        <Resource name="Reported Listings" 
        list={ReportedListings} 
        edit={ListingsEdit} 
        show={ListingShow}
        // create={ListingsCreate}
        // remove={Delete}
        icon={ListingsIcon} 
        remove={Delete} />
        <Resource name="Reported Users" list={UserList} icon={UserIcon} />
    </Admin>
);

export default App;
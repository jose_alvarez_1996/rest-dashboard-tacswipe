
import React, { Component } from 'react';
import axios from 'axios';
import { 
    Responsive,
    SimpleList,
    List, 
    //Edit, 
    Create,
    Filter,
    Datagrid, 
    TextField, 
    ChipField,
    BooleanField, 
    ReferenceField,
    // EditButton, 
    // DeleteButton,
    DisabledInput, 
    LongTextInput, 
    ReferenceInput, 
    SelectInput, 
    SimpleForm, 
    TextInput } from 'admin-on-rest';
import EditButton from './EditButton';
import Edit from './Edit';
import DeleteButton from './DeleteButton';

export const ListingsFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <ReferenceInput label="User" source="userId" reference="users" allowEmpty>
            <SelectInput optionText="name" />
        </ReferenceInput>
    </Filter>
);

// export const ReportedListings = (props) => (
//     <List {...props}>
//         <Datagrid>
//             <TextField source="details"/>
//             <TextField source="title"/>
//         </Datagrid>
//     </List>
// );

export class ReportedListings extends Component {
    render() {
    return (<List {...this.props} title="Reported Listings">
        <Datagrid>
            <TextField source="name"/>
            <TextField source="details"/>
            <TextField source="impressions"/>
            <BooleanField source="isSold" label="sold"/>
            <TextField source="owner.name" label="owner"/>
            <TextField source="flagCount" label="reports"/>
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>);
    }
}


export class ListingShow extends Component {
    render() {
        return (<List {...this.props} title="Listing Show">
            <SimpleForm>
            <DisabledInput source="name"/>
            <DisabledInput source="details"/>
            <DisabledInput source="impressions"/>   
            <DisabledInput source="isSold" label="sold"/>
            <DisabledInput source="owner.name" label="owner"/>
            <DisabledInput source="flagCount" label="reports"/>
            </SimpleForm>
        </List>);
    }
}

const ListingsTitle = ({ record }) => {
    return <span>Listing {record ? `"${record.name}"` : ''}</span>;
};


// export class ListingsEdit extends Component {
//     render() {
//     console.log(this.props);
//     return (<Edit {...this.props}>
//         <SimpleForm>
//             <DisabledInput source="name"/>
//             <DisabledInput source="details"/>
//             <DisabledInput source="impressions"/>   
//             <DisabledInput source="isSold" label="sold"/>
//             <DisabledInput source="owner.name" label="owner"/>
//             <DisabledInput source="flagCount" label="reports"/>
//         </SimpleForm>
//     </Edit>);
//     }
// }

export class ListingsEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listing: null,
            loading: false
        }
    }
    componentWillMount() {

    }

    render() {
    console.log(...this.props);
    return (<Edit {...this.props}>
        <SimpleForm>
            <DisabledInput label="Id" source="_id"/>
            <DisabledInput source="name"/>
            <DisabledInput source="details"/>
            <DisabledInput source="impressions"/>
            <DisabledInput source="isSold" label="Sold"/>
            <DisabledInput source="owner.name" label="Owner"/>
            <DisabledInput source="flagCount" label="Reports"/>
        </SimpleForm>
    </Edit>
)
    }
}
// export const ListingsCreate = (props) => (
//     <Create {...props}>
//         <SimpleForm>
//             <ReferenceInput label="User" source="userId" reference="users" allowEmpty>
//                 <SelectInput optionText="name" />
//             </ReferenceInput>
//             <TextInput source="title" />
//             <LongTextInput source="body" />
//         </SimpleForm>
//     </Create>
// );
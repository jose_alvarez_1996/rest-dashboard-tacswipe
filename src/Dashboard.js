import React from 'react';
import { Card, CardHeader, CardText } from 'material-ui/Card';

export default () => (
    <Card style={{ margin: '2em' }}>
        <CardHeader title="Welcome to TacSwipe Dashboard" />
        <CardText>Please select on the options from the right hand side menu.
        If you encounter any errors, please contact the administrator of this page.</CardText>
    </Card>
);
import { AUTH_LOGIN } from 'admin-on-rest';

const BASE_URL = 'https://tacswipeapi.xyz';
const LOGIN_PATH = '/api/auth/login';
const ADMIN_DEVICE = 'admin-console';
export default (type, params) => {
    if (type === AUTH_LOGIN) {
        const email = params.username;
        const password = params.password;
        const deviceID = ADMIN_DEVICE;
        const request = new Request(BASE_URL + LOGIN_PATH, {
            method: 'POST',
            body: JSON.stringify({ email, password, deviceID }),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        })
        return fetch(request)
            .then((response) => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then((res) => {
                localStorage.setItem('access-token', res.accessToken);
                localStorage.setItem('userId', res.user._id);
            });
    }
    return Promise.resolve();
}
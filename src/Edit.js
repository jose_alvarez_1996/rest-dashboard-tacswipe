import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Card, CardText } from 'material-ui/Card';
import ViewTitle from './layout/ViewTitle';
import Title from './layout/Title';
import DefaultActions from './Actions/EditAction';

const BASE_URL = 'https://tacswipe.xyz';
export class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: 0,
            record: props.data,
            error: false
        };
        this.previousKey = 0;
    }

    componentDidMount() {
        console.log(this.props.location);
        console.log("mounting Component");
        this.fetchResource();
        //this.updateData();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.data !== nextProps.data) {
            this.setState({ record: nextProps.data }); // FIXME: erases user entry when fetch response arrives late
            if (this.fullRefresh) {
                this.fullRefresh = false;
                this.setState({ key: this.state.key + 1 });
            }
        }
        if (this.props.id !== nextProps.id) {
            this.updateData(nextProps.resource, nextProps.id);
        }
    }
    /** CUSTOM METHODS **/

    fetchResource() {
        const listingId = this.getBasePath();
        const token = localStorage.getItem('accessToken');
        axios.get(`${BASE_URL}/api/listing/${listingId}`)
            .then((res) => {
                console.log(res)
                // Update the state
                this.setState({ record: res.body})
            })
            .catch((err) => {
                this.setState({ record: null, error: true});
            });
    }
    getBasePath() {
        const { location } = this.props;
        return location.pathname.split('/').slice(0, -1).join('/');
    }

    editResource(newBody) {
    }
    getListingId() {
        const {location} = this.props;
        const arrPath = location.pathname.split('/');
        return arrPath[ arrPath.length - 1];
    }
    defaultRedirectRoute() {
        return 'list';
    }

    updateData(resource = this.props.resource, id = this.props.id) {
        console.log(resource, id, this.getBasePath());
        this.props.crudGetOne(resource, id, this.getBasePath());
    }

    refresh = (event) => {
        event.stopPropagation();
        this.fullRefresh = true;
        this.updateData();
    }

    save = (record, redirect) => {
        this.props.crudUpdate(this.props.resource, this.props.id, record, this.props.data, this.getBasePath(), redirect);
    }

    render() {
        const { actions = <DefaultActions />, children, hasDelete, hasShow, id, isLoading, resource, title, translate } = this.props;
        const { key } = this.state;
        const basePath = this.getBasePath();
        const { data } = this.state;

        const resourceName = `resources.${resource}.name`
        // const defaultTitle = ('aor.page.edit', {
        //     name: `${resourceName}`,
        //     id,
        //     data,
        // });
        const defaultTitle = `${resourceName}`;
        const titleElement = data ? <Title title={title} record={data} defaultTitle={defaultTitle} /> : '';
        // using this.previousKey instead of this.fullRefresh makes
        // the new form mount, the old form unmount, and the new form update appear in the same frame
        // so the form doesn't disappear while refreshing
        const isRefreshing = key !== this.previousKey;
        this.previousKey = key;

        return (
            <div className="edit-page">
                <Card style={{ opacity: isLoading ? 0.8 : 1 }} key={key}>
                    {actions && React.cloneElement(actions, {
                        basePath,
                        data,
                        hasDelete,
                        hasShow,
                        refresh: this.refresh,
                        resource,
                    })}
                    <ViewTitle title={titleElement} />
                    {data && !isRefreshing && React.cloneElement(children, {
                        save: this.save,
                        resource,
                        basePath,
                        record: data,
                        translate,
                        redirect: typeof children.props.redirect === 'undefined' ? this.defaultRedirectRoute() : children.props.redirect,
                    })}
                    {!data && <CardText>&nbsp;</CardText>}
                </Card>
            </div>
        );
    }
}

Edit.propTypes = {
    children: PropTypes.element.isRequired,
    data: PropTypes.object,
    hasDelete: PropTypes.bool,
    hasShow: PropTypes.bool,
    location: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    resource: PropTypes.string.isRequired,
    title: PropTypes.any,
    translate: PropTypes.func,
};

// function mapStateToProps(state, props) {
//     return {
//         id: decodeURIComponent(props.match.params._id),
//         data: state.admin[props.resource].data[decodeURIComponent(props.match.params.id)],
//         isLoading: state.admin.loading > 0,
//     };
// }

// const enhance = compose(
//     connect(
//         mapStateToProps,
//         { crudGetOne: crudGetOneAction, crudUpdate: crudUpdateAction },
//     ),
//     translate,
// );

export default Edit;
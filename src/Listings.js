import React, { Component } from 'react';
import { 
    List, 
    // Edit, 
    Filter,
    Datagrid, 
    TextField, 
    BooleanField, 
    ReferenceInput, 
    DateField,
    SelectInput, 
    TextInput } from 'admin-on-rest';
import EditButton from './EditButton';
import Edit from './Edit';
import DeleteButton from './DeleteButton';

export const ListingsFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <ReferenceInput label="User" source="userId" reference="users" allowEmpty>
            <SelectInput optionText="name" />
        </ReferenceInput>
    </Filter>
);

export class allListings extends Component {
    render() {
        return (<List {...this.props} title="Listings">
        <Datagrid>
            <TextField source="name"/>
            <TextField source="details"/>
            <DateField source="createdAt" label="Date Created" />
            <TextField source="impressions"/>
            <BooleanField source="isSold" label="sold"/>
            <TextField source="owner.name" label="owner"/>
            <TextField source="flagCount" label="reports"/>
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>);
    }
}
